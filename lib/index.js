#!/usr/bin/env node
'use strict';

const path = require('path');
const homedir = require('os').homedir();
const social = require('@iota-sm/iota-social')('SOCIALCLI', path.join(homedir, '/.social-cli'), 'FileSync');
// const social = require('../../iota-social')('SOCIALCLI', path.join(__dirname, '../../iota-social/.social-cli'), 'FileSync');
const commander = require('commander');
const inquirer = require('inquirer');

const accountRequired = () => {
    if (social.initRequired) {
        console.log('Please create an account first.');
        process.exit();
    }
};

const loadingSpinner = () => {
    let P = ['\\', '|', '/', '-'];
    let x = 0;
    return setInterval(() => {
        process.stdout.write('\r' + P[x++]);
        x &= 3;
    }, 250);
};

const startSpinner = () => {
    return loadingSpinner();
};

const stopSpinner = instance => {
    clearInterval(instance);
    process.stdout.write('\r  \r');
};

const program = new commander.Command();
program.version(require('../package.json').version);
program.description('Social CLI - Lightweight, Command Line social media application using IOTA Social');

program.option('-v, --verbose', 'output extra debugging information');

program
    .command('post [message]')
    .alias('p')
    .description('Post a message to your channel')
    .action(async message => {
        accountRequired();
        if (!message) {
            await inquirer.prompt([{
                type : 'input',
                name : 'message',
                message : 'Enter a message:'
            }]).then(answer => {
                message = answer.message;
            });
        }
        try {
            console.log('///////////////////////');
            let spinner = startSpinner();
            social.feed.send(message, root => {
                stopSpinner(spinner);
                console.log('Posted!');
                if (program.verbose) {
                    console.log('Message posted at ' + root);
                }
            });
        } catch (e) {
            console.log('Something went wrong when trying to post your message.');
            if (program.verbose) {
                console.log(e);
            }
        }
    });
/*
program
    .command('fetch [username]')
    .description('Fetch posts from a channel. Empty username value will fetch your posts.')
    .action(async username => {
        accountRequired();
        try {
            let spinner = startSpinner();
            social.profile.find(username).then(results => {
                stopSpinner(spinner);
                results.forEach(result => {
                    console.log(result);
                });
            });

            social.feed.fetchChannel(root, (post, error) => {
                if (error) {
                    console.log(error);
                } else {
                    console.log(`Message: ${post['data']}\nTimestamp: ${post['timestamp']}\n`);
                }
            });
        } catch (e) {
            console.log('Something went wrong when trying to fetch messages.');
            if (program.verbose) {
                console.log(e);
            }
        }
    });
*/
program
    .command('feed')
    .alias('F')
    .description('Fetch posts from all subscribed channels.')
    .action(async () => {
        accountRequired();
        try {
            social.feed.fetchAll((post, error) => {
                if (error) {
                    console.log(error);
                } else {
                    const author = social.profile.get(post['author']).info;
                    console.log(`Author: ${author.firstName} ${author.lastName}\nMessage: ${post['data']}\nTimestamp: ${post['timestamp']}\n`);
                }
            });
        } catch (e) {
            console.log('Something went wrong when trying to fetch messages feed.');
            if (program.verbose) {
                console.log(e);
            }
        }
    });

program
    .command('following')
    .description('List users you are following.')
    .action(() => {
        accountRequired();
        const following = social.profile.followingDetails();
        console.log(following);
    });

program
    .command('follow [username]')
    .alias('f')
    .description('Follow a user and subscribe to their primary channel.')
    .action(async username => {
        accountRequired();
        if (!username) {
            await inquirer.prompt([{
                type: 'input',
                name: 'username',
                message: 'Enter a username:'
            }]).then(answer => {
                username = answer.username;
            });
        }
        try {
            console.log('///////////////////////');
            let spinner = startSpinner();
            social.profile.find(username).then(results => {
                const me = social.profile.me();
                const following = social.profile.following();
                const filteredResults = [];
                results.forEach(result => {
                    if (following.includes(result.alias)) {
                        console.log(`Already following ${result.alias}`);
                    } else if (!result.alias.endsWith(me.hash)) {
                        filteredResults.push(result);
                    }
                });
                if (filteredResults && filteredResults.length > 0) {
                    stopSpinner(spinner);
                    inquirer.prompt([{
                        type: 'list',
                        name: 'user',
                        message: 'Who would you like to follow?',
                        choices: () => {
                            const choiceObjects = [];
                            filteredResults.forEach(result => {
                                choiceObjects.push({
                                    name: `${result.info.firstName} ${result.info.lastName} - ${result.alias}`,
                                    value: result,
                                    short: `${result.info.firstName} ${result.info.lastName} - ${result.alias.slice(0, 23)}`
                                });
                            });
                            return choiceObjects;
                        }
                    }]).then(answer => {
                        try {
                            let spinner = startSpinner();
                            social.follow(answer.user.alias, answer.user.channels.primary.root).then(() => {
                                stopSpinner(spinner);
                            }).catch(error => {
                                console.log(error);
                            });
                        } catch (e) {
                            console.log('Something went wrong when trying to follow.');
                            if (program.verbose) {
                                console.log(e);
                            }
                        }

                    });
                } else {
                    console.log('Could not find anyone with that username');
                }
            });
        } catch (e) {
            console.log('Something went wrong when trying to follow.');
            if (program.verbose) {
                console.log(e);
            }
        }
    });

program
    .command('find [username]')
    .description('Find someone using their username')
    .action(async username => {
        accountRequired();
        if (!username) {
            await inquirer.prompt([{
                type: 'input',
                name: 'username',
                message: 'Enter a username:'
            }]).then(answer => {
                username = answer.username;
            });
        }
        try {
            console.log('///////////////////////');
            let spinner = startSpinner();
            const me = social.profile.me();
            social.profile.find(username).then(results => {
                stopSpinner(spinner);
                results.forEach(result => {
                    console.log();
                    if (result.alias.endsWith(me.hash)) {
                        console.log('This is you:');
                    }
                    console.log(result);
                });
            });
        } catch (e) {
            console.log('Something went wrong when trying to find users.');
            if (program.verbose) {
                console.log(e);
            }
        }
    });

program
    .command('signup')
    .description('Create a Social CLI account.')
    .action(() => {
        if (social.initRequired) {
            console.log('Welcome to Social CLI! Let\'s start by creating a profile.');
            inquirer.prompt([
                {
                    type: 'input',
                    name: 'username',
                    message: 'Enter a username:'
                },
                {
                    type: 'input',
                    name: 'firstName',
                    message: 'What is your first name?'
                },
                {
                    type: 'input',
                    name: 'lastName',
                    message: 'What is your last name?'
                },
                {
                    type: 'list',
                    name: 'gender',
                    message: 'What gender do you identify as?',
                    choices: [
                        'Male',
                        'Female',
                        'Other',
                        'Rather not say'
                    ],
                    filter: gender => { return gender === 'Rather not say' ? null : gender; }
                }
            ]).then(async response => {
                const username = response.username;
                delete response.username;

                console.log(`Hi ${username}!\nPlease wait while we create your account...`);
                await social.initProfile(`Hi, I'm ${response.firstName}! I started using Social CLI today!`, username, response);
                console.log('Profile created');
            });
        } else {
            console.log('Account already exists.');
        }
    });

program.on('--help', () => {
    console.log('\nExamples:');
    console.log('  $ sc --help');
    console.log('  $ sc post "My first post"');
    console.log('  $ sc F');
});

// error on unknown commands
program.on('command:*', function () {
    console.error(`Invalid command: ${program.args.join(' ')}\nSee --help for a list of available commands.`);
    process.exit();
});

if (!social.initRequired) {
    social.feed.init();
}
program.parse(process.argv);

if (!process.argv.slice(2).length) {
    program.help();
}
