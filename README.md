# Social CLI 💻

A Lightweight Command Line based social media application using Node and IOTA Social.

Please note that this project is still in the early stages of development, and is not fully usable yet.
I would however appreciate any feedback or suggestions.

## Prerequisites

- NodeJS >= 8

## Installation

1. Run `npm config set @iota-sm:registry https://gitlab.com/api/v4/packages/npm/` to add the iota-sm GitLab npm registry
2. Run `npm i -g @iota-sm/social-cli` to install Social CLI globally
3. Unfortunately there is a bug with the Gitlab npm registry which results in dependencies not being installed. The temporary workaround is to install dependencies manually. Run `npm i --prefix=$(npm list -g | head -n 1)/node_modules/\@iota-sm/social-cli/` twice to get it to work.
4. Run `sc --help` to start the program and list the available commands and options
5. Run `sc signup` to create your user profile and start using Social CLI

## Authors

* [Kallahan23](https://github.com/Kallahan23)

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details
